package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a number whose factorial will be computed: ");
        int num = 1;
        try{
            num = scanner.nextInt();
        }
        catch(Exception e){
            System.out.println("Invalid Input!");
            e.printStackTrace();
        }
        finally {
            int answer = 1;

            if(num < 1 ){
                System.out.println("Invalid Input !");
            } else {
                int x = 1;
                while(x <= num){
                    answer = answer * x;
                    x++;
                }
                System.out.println("Tha Factorial of " + num + " is " + answer);
            }
        }


    }
}
